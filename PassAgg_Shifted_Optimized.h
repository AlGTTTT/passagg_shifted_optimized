#define _CRT_SECURE_NO_DEPRECATE // 1
#define _CRT_NONSTDC_NO_DEPRECATE // 1

#include <stdio.h>
#include <math.h>
#include <stdlib.h>
#include <ctype.h>
#include <time.h>
//#include <iostream.h>
#include <string.h>
//#include <iomanip.h>
//#include <fstream.h>

///////////////////////////////////
 #define COMMENT_OUT_ALL_PRINTS

#define SUCCESSFUL_RETURN 0 //2 and (-2) are also normal returns
#define UNSUCCESSFUL_RETURN (-1)

#define INITIAL_TEST_TRAN_DATASETS_INCLUDED 

//#define CALCULATING_Z_SCORES_OF_ALL_FEAS

	#define USE_NORMALIZATION_TO_MEAN_0_AND_STDEV_1

//#define USE_JUST_TRAINING_FOR_MODEL_EFFICIENCY

//#define USE_SUM_OF_TRAINING_AND_TESTING_FOR_MODEL_EFFICIENCY

#define USE_WIGHTED_SUM_OF_TRAINING_AND_TESTING_FOR_MODEL_EFFICIENCY

#ifdef USE_WIGHTED_SUM_OF_TRAINING_AND_TESTING_FOR_MODEL_EFFICIENCY

#define fWeight_Train 0.7
#define fWeight_Test 0.3

#endif //#ifdef USE_WIGHTED_SUM_OF_TRAINING_AND_TESTING_FOR_MODEL_EFFICIENCY



//#define READING_A_MODEL_IN_MAIN
//#define TESTING_TRAIN_VECS_WITHOUT_UPDATING_BY_READING_A_MODEL

//#define TESTING_TEST_VECS_WITHOUT_UPDATING_BY_READING_A_MODEL
//#define TESTING_TRAIN_VECS_WITHOUT_UPDATING

	//#define INITIAL_ORTHONORMALIOZATION
//#define NORMALIOZATION_IN_OrthonormalizationOfVectorsByGrammSchimdt

//#define EXIT_AFTER_TRAINING
///////////////////////////////////////////////

#define nLarge 1000000
#define fLarge 1.0E+12 //9

#define fLarge_ForReading 10.0

#define fZ_ScoreLimit 1.5

#define NO_VecInit
//#define PRINT_DEBUGGING_INFO
#define eps 1.0E-9

#define fLimitForOrthogonalization 1.0E-1

#define fFeaMin (-5000.0)
#define fFeaMax 5000.0

//#define nLengthOneLineMax 1000 //400 //200 //120000 //50000
#define nLengthOneLineMax 2000 //400 //200 //120000 //50000

#define nInputLineLengthMax (nLengthOneLineMax) //200 //100
#define nSubstringLenMax 20
///////////////////////////////////////////////////

//#define nNumVecTrainTot 4178 // for svmguide1_train_2000_2178.txt
//#define nNumVecTrainTot 3089 // for svmguide1_train.txt
//#define nNumVecTrainTot 5267 //3089
//#define nNumVecTrainTot 150 //for svmguide1_train_Pos49_Neg101.txt
#define nNumVecTrainTot 673 //(337+336) // for svmguide1_train_2000_2178.txt


//#define nNumVecTestTot 4000
#define nNumVecTestTot 120 //(60 + 60)

////////////////////////////////////////////////////////////////////
//change 'nLengthOneLineMax' as well
#define nDim 10 //4 //8000
//#define nDim 20 //10 //4 //8000
//#define nDim 50
//#define nDim 100

#define nDim_WithConst (nDim + 1) //8000

#define nDim_D (nDim)
#define nDim_D_WithConst (nDim + 1)

#define nProdTrainTot (nDim*nNumVecTrainTot)
#define nProdTestTot (nDim*nNumVecTestTot)

#define nProd_WithConstTrainTot (nDim_WithConst*nNumVecTrainTot)
#define nProd_WithConstTestTot (nDim_WithConst*nNumVecTestTot)
///////////////////////////////////////////////////////////////////////////
//dimension of the nonlinear/transformed space
//#define nDim_H 1024
//#define nDim_H 512
//#define nDim_H 256
//#define nDim_H 128 
//#define nDim_H 64 //5 //dimension of the nonlinear/transformed space
//#define nDim_H 32
//#define nDim_H 16
//#define nDim_H 8
//#define nDim_H 4
//#define nDim_H 3
//#define nDim_H 2

//#define nDim_H_Max 1024  // should be adjusted depending on the number of features
#define nDim_H_Max 128 //512 //128 //256 // should be adjusted depending on the number of features
//#define nDim_H_Max 32 //128 //256 // should be adjusted depending on the number of features

//#define nDim_H_Min 64 //64//4
#define nDim_H_Min 8 // should be adjusted depending on the number of features
//#define nDim_H_Min 256 // should be adjusted depending on the number of features


#define nDim_H_Factor 2
/*
//#define nNumOfHyperplanes 7200  
//#define nNumOfHyperplanes 3600  
//#define nNumOfHyperplanes 1800  
//#define nNumOfHyperplanes 960  

//#define nNumOfHyperplanes 480  
//#define nNumOfHyperplanes 240  
//#define nNumOfHyperplanes 120  
//#define nNumOfHyperplanes 80  
//#define nNumOfHyperplanes 50  
//#define nNumOfHyperplanes 30  
//#define nNumOfHyperplanes 15  // the number of hyperplanes per one nonlinear fea
//#define nNumOfHyperplanes 8  // the number of hyperplanes per one nonlinear fea
//#define nNumOfHyperplanes 4 //3 // the number of hyperplanes per one nonlinear fea
#define nNumOfHyperplanes 3 // the number of hyperplanes per one nonlinear fea
//#define nNumOfHyperplanes 2 //3 // the number of hyperplanes per one nonlinear fea
#define nK (nNumOfHyperplanes) // the number of hyperplanes

*/

#define nNumOfHyperplanes_Min 2 //4 //3
//#define nNumOfHyperplanes_Max 64 //128 //256 //512
//#define nNumOfHyperplanes_Max 32 //128 //256 //512
//#define nNumOfHyperplanes_Max 8 //16 
#define nNumOfHyperplanes_Max 4 
//#define nNumOfHyperplanes_Max 2 

#define nNumOfHyperplanes_Factor 2

//nK_Min >= nK_Factor
#define nK_Min (nNumOfHyperplanes_Min)
#define nK_Max (nNumOfHyperplanes_Max)
#define nK_Factor (nNumOfHyperplanes_Factor)

////////////////////////////////////////////////////
//for model reading
#define nDim_D_Read (nDim_D)
#define nDim_D_WithConst_Read (nDim_D_WithConst)

#define nDim_H_Read 16 //8
#define nK_Read 2

#define nDim_U_Read  (nDim_D_WithConst_Read * nDim_H_Read*nK_Read)
/////////////////////////////////////////////////////////////////
//#define nNumOfItersOfTrainingTot 6400
//#define nNumOfItersOfTrainingTot 3200
//#define nNumOfItersOfTrainingTot 1600
//#define nNumOfItersOfTrainingTot 800 //99.8%/8.25% at 8-15-800 with srand(3);
//#define nNumOfItersOfTrainingTot 400 //?
//#define nNumOfItersOfTrainingTot 200 //?
//#define nNumOfItersOfTrainingTot 100 //?
//#define nNumOfItersOfTrainingTot 60 //?

//#define nNumOfItersOfTrainingTot 40 //?
//#define nNumOfItersOfTrainingTot 20 //
//#define nNumOfItersOfTrainingTot 10 //25 //15 
//#define nNumOfItersOfTrainingTot 6 

//#define nNumOfItersOfTrainingTot 8 //10
#define nNumOfItersOfTrainingTot 4 
//#define nNumOfItersOfTrainingTot 2 //3 //10#define nNumOfItersOfTrainingTot 1 //10 //5 
//#define nNumOfItersOfTrainingTot 1
//////////////////////////////////////////////////////////////////
//#define fW_Init_Min (-2.0) 
//#define fW_Init_Max 2.0

//#define fW_Init_Min (-1.0) 
//#define fW_Init_Max 1.0

//#define fW_Init_Min (-0.1) //(-1.0) 
//#define fW_Init_Max 0.1 //(1.0) 

//#define fU_Init_HalfRange_Min 0.1 -- would be better
#define fW_Init_HalfRange_Min 0.5


#define fW_Init_HalfRange_Max 5.0
//#define fW_Init_HalfRange_Max 3.0
//#define fW_Init_HalfRange_Max 4.0

#define fW_Init_HalfRange_Step 0.5

/////////////////////////////////
//#define nDim_U (nDim_D_WithConst*nDim_H*nK)

/////////////////////////////////////////////////////////
//#define fU_Init_Min (-4.0) --the last
//#define fU_Init_Max 4.0 

//#define fU_Init_Min (-2.0) 
//#define fU_Init_Max 2.0 

//#define fU_Init_Min (-1.0) 
//#define fU_Init_Max 1.0 

//#define fU_Init_Min (-0.5) //(-1.0) 
//#define fU_Init_Max 0.5 //(1.0) 

//#define fU_Init_Min (-0.1) //(-1.0) 
//#define fU_Init_Max 0.1 //(1.0) 

#define fU_Init_HalfRange_Min 0.5
#define fU_Init_HalfRange_Max 5.0
//#define fU_Init_HalfRange_Max 4.0
//#define fU_Init_HalfRange_Max 3.0
//#define fU_Init_HalfRange_Min 0.1 -- would be better

#define fU_Init_HalfRange_Step 0.5
//////////////////////////////////////////////
//#define INCLUDE_BIAS_POS_TO_NEG
// change 'nNumVecTrainTot' as well
//#define fBiasPosOrNeg (1.0) //1.0 -- no bias,  > 1.0 -- a positive bias; < 1.0 -- negative bias

#define fBiasForClassifByLossFunction 0.0
//#define fBiasForClassifByLossFunction (-0.1)

//#define fBiasForClassifByLossFunction 0.1
//#define fBiasForClassifByLossFunction 0.2
//#define fBiasForClassifByLossFunction 0.3

//#define fFeaConstInit 1.0 //0.5?
//#define fFeaConstInit_Glob_Min (-4.0) 
#define fFeaConstInit_Glob_Min (0.5) 
//#define fFeaConstInit_Glob_Min (1.0) 
//#define fFeaConstInit_Glob_Min (1.5) 
//#define fFeaConstInit_Glob_Max (4.0) 
#define fFeaConstInit_Glob_Max (5.0) 

#define fFeaConstInit_Step 0.5 
//#define fFeaConstInit_Step 0.2 
//#define fFeaConstInit_Step 1.0 //0.1 

////////////////////////////////////////

#define fAlpha 0.9 //const float fAlphaf, // < 1.0
#define fEpsilon 0.05 //?? -- linearly depends on variance

#define fCr 0.125
#define fC 0.125

//////////////////////////////////
#define nSrandInit_Glob_Min 1

//#define nSrandInit_Glob_Max 20 //10
//#define nSrandInit_Glob_Max 100 //50
#define nSrandInit_Glob_Max 200 
//#define nSrandInit_Glob_Max 400 

#define nSrandInit_Step 1

//#define nSrandInit 2
///////////////////////////////////////////////////////////////////

#define fScalarProdOfNormalizedHyperplanesMax 0.1

///////////////////////////
//#define fR_Const 0.61803399
#define fC_Const ( (-1.0 + sqrt(5.0) )/2.0 ) //(1.0-R)
#define fR_Const (1.0 - fC_Const)

#define SHFT(a,b,c,d) (a)=(b);(b)=(c);(c)=(d);

#define SHFT2(a,b,c) (a)=(b);(b)=(c);
#define SHFT3(a,b,c,d) (a)=(b);(b)=(c);(c)=(d);

#define fEfficOfArr1stRequired 0.9

const float fPrecisionOf_Golden_Search = 0.01; // 0.005
const float eps_thesame = 1.0E-5; //-2; // precision of the position
/////////////////////////////////////

typedef struct
{
	int nNumOfPosit_Y_Totf;
	int	nNumOfNegat_Y_Totf;

	int	nNumOfCorrect_Y_Totf;
	int	nNumOfVecs_Totf;

	int	nNumOfPositCorrect_Y_Totf;
	int	nNumOfNegatCorrect_Y_Totf;

	float fPercentageOfCorrectTotf;
	float fPercentageOfCorrect_Positf;
	float fPercentageOfCorrect_Negatf;

} PAS_AGG_MAX_OUT_RESUTS;

